import React from 'react';


function Titleimg(props){
    return(
            <div className="tiles-item">
               <div className="tiles-item-sec">
                   <div className="tiles-item-inner">
                        <img src={props.Titlecardimg} alt="myPic" className="card1-sec" />
                            <p>
                                {props.Titlecardpara1}<br />{props.Titlecardpara2}<br />{props.Titlecardpara3}
                                <br />{props.Titlecardpara4}<br />{props.Titlecardpara5}<br />{props.Titlecardpara6}
                                <br /> {props.Titlecardpara7}<br />{props.Titlecardpara8}
                            </p>
                                <hr className="new2"></hr>
                                    <div className="testimonial-item">
                                        <h6>{props.test1}<span className="line-sec">{props.test2}</span><span className="App-sec">{props.test3}</span></h6>
                                    </div>
                    </div>
                </div>
            </div>
     );
  };

export default Titleimg;


